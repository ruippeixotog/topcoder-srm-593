import java.util.HashSet;
import java.util.Set;

public class HexagonalBoard {

	int[][] color;

	public int minColors(String[] board) {
		color = new int[board.length][board.length];

		int colors = 0;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if (board[i].charAt(j) == 'X' && color[i][j] == 0) {
					boolean found = false;
					for (int k = 1; k <= 6; k++) {
						System.err.println("Trying k = " + k);
						if (floodFill(board, k, i, j)) {
							if (colors < k)
								colors = k;
							found = true;
							break;
						}
					}
					if (!found)
						return 7;
				}
			}
		}
		return colors;
	}

	private boolean valid(String[] board, int i, int j) {
		return i >= 0 && j >= 0 && i < board.length && j < board.length;
	}

	private boolean floodFill(String[] board, int k, int i, int j) {
		if (i < 0 || j < 0 || i >= board.length || j >= board.length
				|| board[i].charAt(j) != 'X' || color[i][j] > 0)
			return true;

		Set<Integer> invalid = new HashSet<Integer>();
		if(valid(board, i, j - 1)) invalid.add(color[i][j - 1]);
		if(valid(board, i, j + 1)) invalid.add(color[i][j + 1]);
		if(valid(board, i - 1, j)) invalid.add(color[i - 1][j]);
		if(valid(board, i - 1, j + 1)) invalid.add(color[i - 1][j + 1]);
		if(valid(board, i + 1, j)) invalid.add(color[i + 1][j]);
		if(valid(board, i + 1, j - 1)) invalid.add(color[i + 1][j - 1]);

		for (int c = 1; c <= k; c++) {
			if (!invalid.contains(c)) {
				color[i][j] = c;
				if (floodFill(board, k, i, j - 1)
						&& floodFill(board, k, i, j + 1)
						&& floodFill(board, k, i - 1, j)
						&& floodFill(board, k, i - 1, j + 1)
						&& floodFill(board, k, i + 1, j)
						&& floodFill(board, k, i + 1, j - 1))
					return true;
			}
		}
		color[i][j] = 0;
		return false;
	}
}
